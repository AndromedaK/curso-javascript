// Funciones Normales o Clásicas
function saludo(){
    return 'hola';
}

// Funciones anónimas tengo que asginarla a una variable
const funcionAnónima = function(){
    return "soy una función anónima";
}

// Funciones de flecha

const funcionFlecha = () => {}
let funcionFlecha2 = () => {}


const darSaludoFlecha = (param1, param2) =>{
    const saludar =  `hola ${param1}, ${param2}`
    return saludar; 
}
const darSaludoFlechaResumida = (param1, param2) => `hola ${param1}, ${param2}`.toUpperCase();

const decirEdad = edad => `tu edad es ${edad}`

const decirNombre = nombre => `tu nombre es ${nombre}` 

const esMayor = ( valor1, valor2)=> valor1> valor2 ? 'Si': 'No';

const obj = {
    var1: {
        var2: {
            var3:3 
        }
    }
}

// Optional Chaining Operator 
const accederVar3 = (obj) => obj?.var1?.var2?.var3
const accederVar3Desestructuracion = ({var1: {var2: var3}}) => var3;

accederVar3Desestructuracion(obj);

const cuadrado = lado => {
    lado * lado
}
// No funciona el return directo con switch case 
const switchCase = (valor) =>{
    switch (valor) {
        case 1:
            return 'El valor es 1'            
            break;
        case 2:
            return 'El valor es 2'
            break;
        default:
            return `El valor es ${valor}`
            break;
    }
}

const evaluar = (valor) => valor && valor*2

// THIS implicito

var nombre = 'Chrome';
var apellido = 'window';

const persona = {
    nombre: 'julio',
    apellido: 'Orellana',
    lista: ['perros', 'gatos','canarios'],
    nombreCompleto: function(){
        return function(){
            console.log(`${this.nombre} ${this.apellido}`)
        }
    },
    nombreCompletoFlecha: function(){
        return () => console.log(`${this.nombre} ${this.apellido}`)
    },
    dameElPerro: function(){

    }
}

// Funcion vacía 
const funcionVacia = () =>{}

function llamarLista(nombre, edad , callback = ()=>{}){
    callback();
    return `${nombre} ${edad}`;
}

// Métodos de Array

const lista  = ["1","2",2,false];

// retorna un arreglo con los resultados
lista.filter(item => item === "1")
lista.filter(item => item === '2')
lista.filter(item => typeof item ==='string');
lista.filter(item => item == 2);

const hijos = {
    hijo1: 'juan',
    hijo2: 'juanita',
    hijo3: 'andres'
}

Object.values(hijos).filter(hijo => hijo ==='juan')
Object.values(hijos).filter(hijo => hijo.startsWith('juan'))


/***Pruebas con Reduce */
const orderSandwichOrdersByDate = () => sandwichOrders.reduce(ordersByDate,{} )
const ordersByDate =  (dates,currentItem)=>{

    // Desestructuración de la fecha de la orden actual
    const { ordered } = currentItem
    
    debugger; 
    console.log([ordered]);

    const { [ordered]: currentDateList = []  } = dates;
 
    dates[ordered] = [...currentDateList, currentItem];

    return dates; 

}

const reducido =  [1,2,3,4,5,6,7,8,9,"4"].reduce((acu, valorac) => { return acu + valorac }, 0) 

const numeros  = [1,2,3,4,5,6];
const reducerNumerosArray = numeros
                            .reduce((acumulador, numeroActualdelArray)=> acumulador + numeroActualdelArray, 0)
