//Programación Funcional

// -> Función Pura - Ayuda al memoize 
// Mejor desarrollo - Mejor Testing

/**
 *      
 * @param {*} a Primer valor a multiplicar
 * @param {*} b Segundo valor a multiplicar
 * @return el Resultado  
 */

function multiplicar(a,b){
    return a*b;
}

function multiplicar2(a,b){
    if (a>5) {
        return a*b
    }
    return 0;
}
// -> Función Impura

function saludar(nombre){
    var saludo = "Hola "+ nombre +" Hoy es" + new Date().toLocaleDateString()
    return saludo
}

function callConsole(){
    console.log("Hola")
}


// Mutabilidad 
// Es parte de una función impura

var persona = {
    nombre: "cristopher",
    edad: 12
}

function changeAge(persona){
    persona.edad = persona.edad + 2
    console.log(persona)
}

// Inmutabilidad
// no modificar el objeto original sino retornar uno nuevo
function nuevoObjeto(){
    var nuevaPersona = {
        nombre : persona.nombre,
        edad: persona.edad +1
    }
    return  nuevaPersona; 
}


var arreglo = [1,2,3,4]

function nuevoArreglo(){
    var nuevoArreglo = arreglo.slice(2);
    return nuevoArreglo;
}

var height = 20;

// Son funciones uqe el resultado será distinto con los mismos argumentos
function calcularAltoImpura(valorAltoBase){
    return valorAltoBase * height
}

// Son funciones que el resultado será siempre el mismo con los mismos argumentos
function calcularAltoPura( valor, altura){
    return valor * altura; 
}

// Let - Const

function tryvar(){
    let x = 5;
    if (x == 5) {
        let x =10
    }
    console.log(x)
}

let nombreLet = 'link'

// No se pueden redefinir las constantes
// pero si se puede mutar lo interno 

const objetoConst = {};
const arregloConst = [];
const miFuncion = function(){}

//Arreglos con Const 

const perros = []
const gatos = []
const pandas = []
const oso = []

// Funciones con Const

const comer = function(){}
const beber = function(){}
const hablar = function(){}
const correr = function(){}


// Triple Igualación 

1 == 1;    // true
1..toString() == '1'; // true

1 ===   1; // true
1 === '1'// false 

// Parámetro por default

//const nombre = 'Julio Orellana';
const profesion = 'desarrollador'
const edad  = 31;
const gustos = ["perros","gatos"]

const armarFicha = function(
    nombre, 
    profesion = "(sin Profesion)",
    edad = "(sin edad)"
    ){
    return  `
        Nombre:  ${nombre}, 
        Profesion: ${profesion}`;
}

// Spread Operator se utiliza principalmente para dividir 

const texto = "text0";
const textoArreglo = [...texto] // genera un arreglo con ['t','e',...]
const textoObjeto  = {...texto} // genera un objeto con {0:'t',1:'e'}


// Spread en Objetos
const propiedades = {a:'a'};
const masPropiedades = {b:'b',c:'c'}

const nuevaPropiedad = {...propiedades, ...masPropiedades} // desarmo los objetos {a:'a', b:'b', c:'c'}

const nuevaPropiedades = {
    ...propiedades,
    d:'d',
    c:'10'
}

// Spread en Arreglos
const mascotas = ['conejos', 'gatos','peces']
const masMascotas = ['perros', 'canarios']

const nuevoArregloSpread = ['osos', ...masMascotas, ...masMascotas]

// Spread en funciones

const funcionSpread = function(a,b,...restoNumeros){
    console.log(a,b,restoNumeros)
}

// Desestructuración de objetos

const desarrollador = {
    nombre: "cristopher",
    profesion: {
        titulo: "ingeniero",
        cargo: "desarrollador"
    },
    edad: 32,
    gustos: ["gatos","perros"],
}

const {
    nombre, 
    profesion:profesion2,
    edad: edad2, 
    gustos:gustos2 } = desarrollador;


const {titulo, cargo} = profesion

// Desestructuración de arreglos


var a;
var b = (a =3)? true: false