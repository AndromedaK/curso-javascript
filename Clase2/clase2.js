function cuadrado(numero){
  return numero * numero;                                                          
}

function formateo(nombre, apellido){
  return nombre + ' ' + apellido;
}

function perimetroCirculo(radio){
 return Math.PI * radio * 2;
}

function areaCirculo(area){
 return Math.floor( Math.PI * (area**2));
}

function numeroAleatorio(){
  return Math.random();
}

function llamadaVoid(){

}

function llamadaNoVoid(){
 return 'hola';
}

function fechaActual(){
 var fechaActual = new Date();
 var regex = RegExp('/','g')
 return fechaActual.toLocaleDateString().replace(regex,'-');
}

function fechaYHoraActual(){
 return new Date().toLocaleString();
}


var functionDeclarativa = function(){}

// función auto ejecutable

// (
// function (){+
// console.log("Hi")}

// )()


// Curry Functions  -  Closures -  Nested Functions ( Funciones anidadas)
// Acepta multiples argumentos, pero uno a la vez

function sum(a,b){ return a + b}

// Currificar la funcion 
// es una función que retorna a otra función  

function suma (a){
  return function(b){
     return a +b
  }
}

function multiplica(a){
 return function(b){
    return a * b                                                   
 }
}

// suma(2)(3)


function carro(){
 var carroActual = []
 function modificarCarro (item, accion){
  if(accion == "agregar"){
    carroActual.push(item)             
  }
  if(accion == "sacar"){
    carroActual.pop(item)
  }
 }
}

function contador (contadorInicial){
 return function(incremento){
  contadorInicial++
  return contadorInicial = contadorInicial + incremento
 }
}

function contadorAutomatico(contadorInicial){
  return function(){
    return contadorInicial++;
  }
}

// no es closure

function method1(){
 
 // Metodo Interno
 function method2(){
   console.log('metodo2')
 }
  method2();
}


// Es closure
function metodod1(){
 
  function method2(){
    console.log('metodo2')
  }
  return method2();
}


// Hoisting 

// tipos de declaraciones
// esta pensado unicamente para las funciones


var declaracinVariable = false;
function declaracionfuncion(){};

function ejemploHoisting(){

  // declara y después asigna 
  console.log(variableInterna)
  
  // declarar  - asignar
  var variableInterna = 0;
}

// condicionales

// definir comportamiento de un código 
// hacemos algo o hacemos otra cosa

// if ( 3 < 5) -> true
// else 
var a = 7;
var b = 22;

function condicional(){

  if( a > b ){
    return true;
  }
  else if ( a < b){
    return; 
  }
  else{
    return false; 
  }

}

// operador ternario
var resultado = (6>0) ? '6 es mayor' : '6 no es mayor'

function evaluarNombre(nombre){
  return nombre ? nombre : 'Sin nombre'
}

function evaluarNombreCorto(nombre){
  return nombre && nombre 
}


// Switch Case
function sinSwitch( valor){
  if(valor == 0){
    console.log('es 0')
  }
  if(valor == 1){
    console.log('es 1')
  }
  if(valor == 2){
    console.log('es 2')
  }
  if(valor == 3){
    console.log('es 3')
  }
}


function conSwitch(valor){
  switch(valor){
    case 0: console.log(' es 0'); break;
    case 1: console.log(' es 1'); break;
    case 2: console.log(' es 2'); break;
    case 3: console.log(' es 3'); break;
    default : console.log("Es "+valor);
  }
}

// Prototipos

// - Es una mecanismo que  Incorpora la POO. Hereda características entre objetos
// - Los prototipos se crean con mayúsculas 

function Persona(nombre, apellido, edad){
  this.nombre = nombre;
  this.apellido = apellido;
  this.edad = edad; 
}

// - Las funciones de definen afuera
Persona.prototype.obtenerNombre = function(){
  return this.nombre; 
}

Persona.prototype.obtenerApellido = function(){
  return this.apellido;
}

Persona.prototype.obtenerEdad = function(){
  return this.edad; 
}

var persona = new Persona('Cristopher', 'Angulo', 33);


// Iteraciones

// FOR

for ( let valorInicial = 0; valorInicial < 10 ; valorInicial++){
  console.log( "Hola Mundo" + valorInicial); 
}

for ( var inicial = 10; inicial > 0 ;  inicial--){
  console.log( "Xao Mundo"+ inicial);
}

var cars = ["Saab", "Volvo", "BMW"]; 

// se va a repetir mientras el largo de cars sea mayor a valor inicial
for ( var inicial = 0; inicial < cars.length; inicial++){
  console.log( "Auto : "+  cars[inicial]);
} 

var inicial = 0;

while (inicial < 10) {
  console.log( "while incremental", inicial);
  inicial++; 
}

var inicial = 0;

do{
  console.log( "do while incremental");
  inicial++;
}while(inicial < 10){
  
}


