
// Un bloque que nos permte agregar propiedaes al objeto
// Javascript nos permite manipular los objetos fácilmente a través de la mutabilidad
var car = {};
car.color  =  'rojo';
car.weight =  500.0;
car.sold   = false;

// Arrays 
var array = ["0",1, true, function(){}, null, undefined]

// Scope
// El scope en js se refiere al contexto actual del código 
// 2 tipos ; local (dentro de un bloque o funcion ) y global ( objeto window )

var texto = "soy un texto";
console.log(texto._proto_);

function ageFuture(){
    return this.age + 2; 
}


// HashMap : Clave - Valor
// los objetos pueden almacenar cualquier tipo de valor
var persona = {
    age: 18, 
    hasChildrens: undefined,
    nicknames: ['Zelda', 'Enlace'],
    changeName: function(nombre){
        this.nombre = nombre; 
    },
    changeAge: function(age){
        this.age = age;
    },
    firstChild: function(){
        var firstChild = {
            nombre: 'goron',
            giveMeName: function(){
                console.log(this.nombre)
            }
        }
        console.log(firstChild.nombre)
    },
    ageFuture: ageFuture
}
persona.nombre = 'Link';


// Formas de acceder

console.log('persona.nombre: ', persona.nombre);

console.log("persona['nombre']: ", persona['nombre'])

var getProperty = 'nombre'
console.log('persona[getProperty]: ', persona[getProperty])

// Herencia de objetos en JS

Object.prototype.bar = 1; 

var nuevoObjeto = {}
nuevoObjeto.hasOwnProperty('bar') // false - > porque bar viene de Object

Object.keys(persona); // devuelve un arreglo con las llaves del objeto persona
Object.values(persona); // devuelve un arreglo con los valores del objeto
Object.entries(persona); // devuelve un arreglo de arreglos que contiene  llava valor

var animal = {
    bar:1,
    ballena: {
        bar:2,
        humano: {
            bar:3
        }
    }
}

Object.freeze(); // Previene la modificacion del objeto

var heroeFreeze = Object.freeze(persona);
console.log(Object.isFrozen(heroeFreeze)); // preguntar si está freezeado
var stringify = JSON.stringify(heroeFreeze); // Transformar a json

var destrinfy = JSON.parse(stringify);

// Arreglos

var elemento3 = 3;
var conjunto = [ 'elem1', 'elem2',elemento3]

Array // prototipo 
Array(4) // para crear un arreglo con 4 posiciones
Array(1, 23,"ds", true, Array(4))

var mascotas = ['Perros', 'Gatos', 'Canarios', 'Peces']
// Iterando sobre todos los elementos del arreglo
mascotas.forEach(function(mascota, indiceMascota, arrayCompleto) {
    console.log('Mascota: ', mascota, ' es la posición -> ', indiceMascota, ' del arreglo de  ->', arrayCompleto) 
    if(mascota == 'Gatos'){
        console.log("Me gustan los " + mascota)
    }

})

// Para agregar cosas al arreglo - Estamos modificando y mutando el mismo arreglo
mascotas.push('chinchillas');
mascotas.push({
    bar: 1
})
mascotas.push(['hola','chao'])


// .Pop Quitar el ultimo elemento del arreglo

mascotas.pop();


// .shift Eliminar el primero

mascotas.shift();


var mascotas = ['Perros', 'Gatos', 'Canarios', 'Peces', 'Gatos']

// Objetos  de JS

var pinguino = {
    nombre: 'jorge',
    edad: 22
}

var jirafa = {
    nombre: "jirafa",
    edad: 22,
    comer: function(){
        console.log("comer")
    }
}
//  referencia  distinto de ejecución

/**Problemas de referencia */

var var1 = { a: 1} // 0xaabb
var var2 = var1 // 0xaabb

var2.a = 2; // se cambia también el valor de rrefrencia de la misma memoria

// SET : Tiene valores únicos

var arreglo = [1,1,2,3,232,2,3,3];
var miSet = new Set(arreglo); // volvemos los elementos como únicos 
var nuevoArreglo  = Array.from(miSet);


// Sacar Cosas únicas 

var objeto1 = {a: 1}
var objeto2 = {b: 2}
// var arreglo2 = [1,2, {a:1}, {a:2}];
var arreglo2 = [1,2, objeto1, objeto2, objeto2];
var miSet2 = new Set(arreglo2); // volvemos los elementos como únicos 
var nuevoArreglo2  = Array.from(miSet2);

/**SCOPE */

this // Hace referencia al global Objet 

function foo(){
    console.log(this)
}


// ocupamos el this para hacer referencia a las propiedades del objeto
var objeto = {
    propiedad1: 'hola',
    foo: function(){
        console.log(this)
    }
}

var nombre = 'Chrome'
var apellido = 'Window'


// se pierde cuando hay una function dentro de una funcion 
var persona = {
    nombre: 'Julio',
    apellido: 'orellana',
    ncompleto: function(){
        return function(){
            // --> hace referencia al global object 
            console.log(this.nombre, this.apellido)
        }
    },
    ncompletoSolucion1: function(){
        var self = this;
        return function(){
            console.log(self.nombre, self.apellido)
        }
    },
    ncompletoSolucion2: function(){
        
        // vinculamos la referencia dle objeto actual
        function nombreCompleto(){
            console.log(this.nombre, this.apellido)
        }
        // utiliza este scope y no el global objet 
        return nombreCompleto.bind(this)
    }
}

var imprimirNombreCompleto = persona.ncompleto();
var imprimirNombreCompleto1 = persona.ncompletoSolucion1();
var imprimirNombreCompleto2 = persona.ncompletoSolucion2();


var anime = {
    nombre: "Dsfsd",
    apellido: "sdfsdf"
}

// // var funcion1 = function(){
// //     return this.nombre + '  '+  this.apellido;
// // }

// // var nuevafuncion = funcion1.bind(anime)
// // nuevafuncion()


// // // .call definir la variable y pasar los argumentos por coma

// // var funcion2 = function(var1, var2){
// //     console.log('variable', var1, var2)
// //     return this.nombre + ' ' + this.apellido
// // }

// funcion2.call(anime,12,2)

// .apply 

var funcion3 = function(var1, var2){
    console.log('variable', var1, var2)
    return this.nombre + ' ' + this.apellido
}

funcion3.apply(anime, [2,3])