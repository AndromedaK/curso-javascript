/**TAREAS */

/** PARTE 1 */

const lechugapalta = () => {
   let resultado = sandwichOrders.filter(
        sandwich => { return sandwich.ingredients.includes('lechuga') && sandwich.ingredients.includes('palta')}
    )
    return resultado;
}

const retornarOrdenMensaje = (id) => {
    let encontrado = sandwichOrders.find(orden => orden.id === id);
    if (encontrado) {
        const {
            protein,
            ordered,
            bread
        } = encontrado;
        console.log(`La orden fue realizada el ${ordered}, la orden llevó ${protein} y ${bread}`);
    } else {
        console.log(`No sé encontró la orden con id ${id}`)
    }
}

const retornarBooleanoIngrediente = (id) => {
    let orden = sandwichOrders.find(orden => orden.id === id);
    let contiene = orden.ingredients.includes('pepinillos');    
    if(contiene){
        return true;
    }else{
        return false
    }
}

const retornarCantidadOrdenesSegunFecha = (fecha) => {
    const filtrados = sandwichOrders.filter( orden => orden.ordered === fecha);
    return `Se encontraron ${filtrados.length} ordenes para la fecha ${fecha}`
}

const retornarTodasFechas = () => {
    const ordenes = sandwichOrders
                    .filter(
                        orden => orden.protein === 'not burger' && orden.ingredients.includes('cebolla caramelizada'));

    const fechas = ordenes.map(orden => orden.ordered);
   
    table(ordenes);
    table(fechas);

    return fechas;
 } 

 /**PARTE 2 */


/**
 * @returns Cantidad de ordenes por cada pan 
 */
const retornaObjeto = (panes, ordenActual) => {
  
    const { bread } = ordenActual;
 
    const {[bread]: pancitos = []} = panes;

    panes[bread] = [...pancitos, ordenActual];

    return panes; 
};
const resultado = sandwichOrders.reduce(retornaObjeto, {});


