/** TAREAS */

/** PARTE 1 */
function ParOImpar(num){
  if (num % 2 == 0) {
      console.log("El ", num, ": es Par")
  } else {
      console.log("El ", num, ": es Impar")    
  }
}

/** PARTE 2 */
function devolverPosicion(letra){  
    let cadena  = 'abcdefghijklmnñopqrstuvwxyz';
    let posicion =  cadena.indexOf(letra);
    console.log(letra, posicion);
    return posicion; 
}

devolverPosicion('a'); 
devolverPosicion('b'); 
devolverPosicion('c'); 
devolverPosicion('d'); 
devolverPosicion('e'); 


/** PARTE 3 */
function agregarImpuesto(valorOriginal){ 
    let impuesto  = 19;
    let obtener19 = (valorOriginal * impuesto) / 100;
    let valorConImpuesto = valorOriginal + obtener19; 

    console.log(obtener19);
    console.log(valorConImpuesto);

    return `$El precio con impuesto es: ${ valorConImpuesto}`; 
}

agregarImpuesto(1000);

/** PARTE 4 */

function nuevoDado(nombre){
    return function(){
       let randomNumero = Math.floor(Math.random()*6)+1 ;
       return `${nombre} tiró un dado y salió ${randomNumero}`
    }
}


/** PARTE 5 */

function Person(nombreCompleto, edad ){
  this.nombreCompleto = nombreCompleto;
  this.edad           = edad;
}

Person.prototype.guardarEnLocalStorage = function(){

   localStorage.setItem(this.nombreCompleto, this.nombreCompleto);
   localStorage.setItem(this.edad, this.edad);

   let _nombreCompleto = localStorage.getItem(this.nombreCompleto);
   let _edad = localStorage.getItem(this.edad);

   return `${_nombreCompleto} tiene ${_edad} años guardados en local storage`;

}

Person.prototype.guardarEnSessionStorage = function(){
    
    sessionStorage.setItem(this.nombreCompleto, this.nombreCompleto);
    sessionStorage.setItem(this.edad, this.edad);
 
    let _nombreCompleto = sessionStorage.getItem(this.nombreCompleto);
    let _edad = sessionStorage.getItem(this.edad);
 
    return `${_nombreCompleto} tiene ${_edad} años guardados en session storage`;
}