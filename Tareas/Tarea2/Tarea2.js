/** Tareas */

/** PARTE 1 */

let contador = 0;

const tarea2 = {
    mascotas: ['perros','gatos','canarios','pez','serpiente'],
    eliminarMascota: function(tipoMascota){
       let mascota =  this.mascotas.indexOf(tipoMascota);
       this.mascotas.splice(mascota,1);
       return this.mascotas;
    },
    eliminarUltimaMascota: function(){
        this.mascotas.pop();
        return this.mascotas;
    },
    agregarMascota: function(nuevaMascota){
        this.mascotas.push(nuevaMascota);
        return this.mascotas;
    },
    contadorMascotas: function(){
        this.mascotas.forEach(function(mascota){          
            let lastCharacters =  mascota.slice(-2);
            if(lastCharacters.includes("os")){
                contador++
            }       
        })
        console.log(`En el arreglo hay ${contador} mascotas que terminan con 'os' `)
        contador=0;
    }
}

/***PARTE 2 */

const trabajador = {
    nombre: 'Jhon Smith',
    cargo: 'QA',
    empresa: {
        ubicación: {
            comuna: 'Santiago',
            puesto: 'nº 24',
        },
        datos: {
            nombre: 'ACME',
        },
        clientes: ['Facebook', 'Google'],

    },
    gustos: ['comer', 'ver televisión', 'dormir'],
    hijos: null,
    
}

function mensaje1(trabajador){
    const {
        nombre,
        empresa:{
            datos:{
                nombre:nombreEmpresa
            }
        },
        cargo,
        gustos:[primerGusto,...restos],
        hijos
    } = trabajador;

    hijosConvertido = (hijos==null || undefined)? 'No tiene hijos' : hijos;

    let restante = 0;
    restos.forEach(element => {
        restante++
    });

    return `El trabajador (${nombre}) trabaja en (${nombreEmpresa}) 
            con cargo (${cargo}) y le gusta (${primerGusto}) y (${restante}) más, (${hijosConvertido})`
}


const mensaje2 = function(trabajador){
    const {
        nombre,
        empresa:{
            ubicación:{
                comuna,
                puesto
            },
            clientes: [facebook, google]
        },
        cargo,
    } = trabajador;

    return `El trabajador (${nombre}) va a su trabajo en (${comuna}), es (${cargo}), 
            en el puesto (${puesto}), trabaja con (${facebook}) y (${google})`

}   