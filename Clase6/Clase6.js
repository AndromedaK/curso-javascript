//Timers
// son parte de la api de window del navegador 

// setTimeout(callback, tiempo)

setTimeout(function(){
    console.log("Hola 1 segundo")
}, 1000)

// setInterval(() => {
//     console.log( " se ejecuta cada 2 segundos")
// }, 2000);

var variable1 = 200;
var variable2 = "hola"

setTimeout(function(valor1, valor2){
    console.log(valor1, valor2)
}, 2000, variable1, variable2);


function buscarAlgo(){
    console.log("cargando...")
    var promise = new Promise(function(resolve){
        setTimeout(function(){
            resolve("listo")
        }, 5000)
    });

    promise.then(function(respuesta){
        console.log(respuesta)
    })
}

// Como cancelamos la ejecución
// Cancelar Timers

clearInterval();

var timeout = setTimeout(() => {
    console.log("HolaMundo")
}, 10000);

clearTimeout(timeout);

var interval = setInterval(() => {
    console.log("HolaMundo")
}, 2000);

clearInterval(interval);


var timeTimeout;
function startTime(){
    var today = new Date()
    var horas = today.getHours();
    var minutos = today.getMinutes();
    var segundos = today.getSeconds();
    minutos  = agregarCeros(minutos);
    segundos = agregarCeros(segundos);
    document.getElementById('clock').innerHTML = horas + ":" + minutos + ":" +segundos;
    timeTimeout =  setTimeout(startTime, 500)
}
function stopTime(){
    clearTimeout(timeTimeout);
}

function agregarCeros(valor ){
    if (valor<10) {
        return '0' + valor;      
    }
    return valor
}

// Manejo de excepciones

// Evitan que se detengan la ejecución del código

try {
    console.log(variableInexsitente)
} catch (error) {
    console.log(error)
}


// stacktrace el orden de funcionalidades que te llevaron al error
function throwMyException(variable){
    try{
        if (variable=="hola") {
            throw 0
        }
    }
    catch(error){
        console.log(error)
    }
}
function throwMyCustomExeption(variable){
    try {        
        throw Error("fallé con error");
    } catch (error) {
        console.log(error)
    }   
}
function  aaa(){
    throwMyCustomExeption();
}

function exceptionType(value){
    // typerror
    // rangeerror
    // referenceerror

    try {
        // throw new Error("hola")
        // Number.toFixed();
        if(value == 1){
            Number.toFixed();
        }else if(value == 2){
            new Array(invalidasd)
        }else if(value == 3){
            new Array(Math.pow(2,40))
        }else if(value == 4){
            throw new Error("CUSTOM ERROR")
        }
   
    } catch (error) {
        if(error instanceof TypeError){
            console.log("Type Error")
        }else if(error instanceof ReferenceError){
            console.log(" referece error")
        }else if(error instanceof RangeError){
            console.log("Range error")
        }
        console.log(typeof error);
    }
}


function finallyUsar(){
    try{
        throw new Error("fallé")
    }catch(error){
        console.log(error)
    }finally{
        console.log("Fin Loading")
    }
}