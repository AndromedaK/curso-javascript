/*** SINCRONÍA */


// Javascript: Trabaja en un solo hilo de ejecución : hace uan cosa a la vez
// single threaded == onecallstack

// Se ejecutan en orden

var a = 1;
var b = 2;
var c = 3; 


function bloqueador(){

    console.log("inicio");

    var i = 0;
    var start = new Date();

    while( i < 2000000000){ i ++ }
    var end = new Date();
    
    console.log( end, start)

}

/*** PROMESA */

//Es una objeto que tendrá un valor en el futuro
// MicroTaskQueque

// var promise = new Promise(
//     function(resolve, reject){
//         // resolve("resuelta"); 
//         // reject("rechazado");
//         resolve({codigo: 300})
// });

// promise.then(function(resultado){
//         console.log("then 1:", resultado)
//         return 30
// }).then(function(resultado){
//     console.log(resultado)
// }).then(function(resultado){
//     console.log(resultado)
// })


// Cuando la promesa falla

// promise.catch(function(error){
//     console.log("Error: ", error)
// })

console.log("1era ejecucion")
new Promise(function(resolve){
    resolve()
}).then(function(){
    console.log("2da ejecucion")

})
console.log("3era ejecucion")


function callPromise(){
    var result;
    var promise = new Promise(function(resolve){
        resolve("promesa resuelta")
    })

    promise.then(function(respuesta){
        result = respuesta; 
        console.log(result)
    })
    console.log(result); 
}

/** ASYNC - AWAIT */
// ES7

async function callPromiseAsync(){
    var promise = new Promise(function(resolve){
        resolve(50)
    })
    var resultado = await promise;
    console.log(resultado);
}


// Promise.resolve();
// Promise.reject();

//** FETCH */
// Comunicación entre Front y Back
// fetch()
// se hace usualmente con API REST

/**FETCH */

// GET - POST - PUT - DELETE

function testFetch(){
    
    fetch('https://6008ca820a54690017fc2559.mockapi.io/users', { method: 'GET'})
    .then(function(respuesta){
        console.log(respuesta)
    })
}


// fetch('url', { method: 'POST'});


async function testFetch2(){
    
    var respuesta = await  fetch('https://6008ca820a54690017fc2559.mockapi.io/users', { method: 'GET'})
    var resultado = await respuesta.json();
    console.log(resultado)
}


async function getusers(){
    var resp = await fetch('https://6008ca820a54690017fc2559.mockapi.io/users',{method: 'GET'});
    var res  = await resp.json();
    console.log(res);
}

function getallUsers(){
    var url = 'https://6008ca820a54690017fc2559.mockapi.io/users';
    fetch(url, {method: 'GET'})
        .then(function(respusta){
            console.log(respusta);
            return respusta.json();
        })
        .then(function(respuesta){
            console.log(respuesta);
        })
}

function API1(){
    var url = 'https://6008ca820a54690017fc2559.mockapi.io/users';
    return fetch(url)
}

function API2(){
    var url = 'https://6008ca820a54690017fc2559.mockapi.io/users';
    return fetch(url)
}
function API3(){
    var url = 'https://6008ca820a54690017fc2559.mockapi.io/users';
    return fetch(url)
}

function parseIntoJson(respuesta){
    return respuesta.json();
}

// Promise.all

async function callApis(){

    //antes, tenemos que esperar por cada llamada para que avance la siguiente
    // var resp1 = await API1() // 1ms
    // var resp2 = await API2() // 2ms
    // var resp3 = await API3() // 4ms

    // Después
    // desestructuración
    var [resp1, resp2, resp3] = await Promise.all([API1(),API2(), API3()]); // 4ms 

    var [ parseada1, parseada2, parseada3
    ] = await Promise.all([
        parseIntoJson(resp1),
        parseIntoJson(resp2),
        parseIntoJson(resp3)
    ]);

    console.log({resp1, resp2, resp3})
    console.log({parseada1, parseada2, parseada3})

}


// Promise.race
// Primero en responder
// Verificar conexión a internet

async function callRace(){
    var primero = await Promise.race([
      API1(),
      API2(),
      API3()
    ])

    console.log(primero.url)
}

// Promise.resolve
async function resolver(){
    var espuesta = await Promise.resolve("hola");
    console.log(espuesta);
}


