// Browser object Model : BOM Permite a js comunicar con el browser
// Document object Model: DOM representa directament 

var variableExterna = "externa ";

function llamarFunction(){
    var variableInterna = "interna";   
}

// Window

window.closed;
window.history;
window.location;


// Document

// document.getElementById();
// document.getElementByClassName();
// document.querySelector();


function definirContador(){
    var contador = 0;
    return function(){
        return contador++;
    }
}


var elementos    = document.querySelectorAll('.elementoBase');
var contador  = definirContador();

var llamarContador = function(){
    console.log(contador());
}

function addListener(){
    for (let index = 0; index < elementos.length; index++) {
        elementos[index].addEventListener(
          'click',
          llamarContador,
           true
        );  
    }
}

function deleteListeners(){
    for (let index = 0; index < elementos.length; index++) {
        elementos[index].removeEventListener(
            'click',
            llamarContador,
 
            true
        )       
    }
}

function saveVariable(valor ){
    localStorage.setItem(valor, valor);
}

function readVariableSaved(key){
 
    var localstorageValue = localStorage.getItem(key);
    console.log( localstorageValue)
}

function deleteVariableSaved(key){
  localStorage.removeItem(key)
}

function cleanAllLocalStorage(){
    localStorage.clear()
}


// LocalStorgae tiene persistencia
// SessionStorage persiste hasta que se cierra la pestaña