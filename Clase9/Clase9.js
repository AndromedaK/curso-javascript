// Patrón de Diseño Prototipo

const acceptedTypes = ['guerrero','mago','arquero']

/**
 * 
 * @param {object} player - player Information
 * @param {string} name - name of  player 
 * @param {string} type - type  of  player 
 * @param {number} playerNumber -  number of players 
 * @return instance of new player
 */

// Pasar como objeto {}, uno decide el orden y los campos 
const Player = function ({name, type='guerrero', playerNumber}){

    if(!name){
        throw new Error('"name" no puede quedar vacío')
    }
    if (!acceptedTypes.includes(type)){
        throw new Error(`"type" debe ser de tipo [${acceptedTypes.toString()}]`)
    }
    this.name = name
    this.type = type 
    this.playerNumber = playerNumber
    this.currentHP = 1000;
    this.lastAttackDamage = 0;
}

// Mantener el scoped
Player.prototype.doDamage = function(target){
    const damage = `${this.name} hizo ${this.lastAttackDamage} daño a ${target.name}`
    console.log(damage)
    target.currentHP = target.currentHP - this.lastAttackDamage
}

Player.prototype.calculateDamage = function(){
    this.lastAttackDamage = Number(Math.random()*50)
.toFixed(0)}

Player.prototype.isDead= function(){
    return this.currentHP <= 0
}