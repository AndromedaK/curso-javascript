// patrón de diseño Singleton

const logger = {
    printOnConsole: ({log, color='red'}) =>{
        console.log(`%c ${log}`, `color ${color}`)
    }
};

// conglea el objeto no permite que sea cambiante
Object.freeze(logger)